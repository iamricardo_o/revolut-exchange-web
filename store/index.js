import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import account from './account/reducer';
import exchange from './exchange/reducer';

const reducers = {
  exchange,
  account,
};

export default initialState => createStore(
  combineReducers(reducers),
  initialState,
  composeWithDevTools(applyMiddleware(thunkMiddleware)),
);
