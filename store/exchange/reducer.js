import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
  rates: {},
});

export default function reduce(state = initialState, action) {
  switch (action.type) {
    case types.FETCHED_RATES: {
      const newRates = { ...state.rates };
      newRates[action.payload.currency] = { rates: action.payload.rates };

      return Immutable.merge(state, {
        rates: newRates,
      });
    }

    default:
      break;
  }
  return state;
}

/* selectors */

export function getRates(state) {
  return state.exchange.rates;
}
