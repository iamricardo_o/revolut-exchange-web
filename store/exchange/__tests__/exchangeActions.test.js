import fetchMock from 'fetch-mock';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../actions';
import * as types from '../actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('should handle fetchRates', () => {
    const result = { rates: { USD: 0.82, GBP: 1.12 } };

    fetchMock.getOnce('https://api.exchangeratesapi.io/latest?base=EUR', {
      rates: result.rates,
      headers: { 'content-type': 'application/json' },
    });

    const currency = 'EUR';

    const expectedActions = [
      {
        type: types.FETCHING_RATES,
        payload: {
          currency,
        },
      },
      {
        type: types.FETCHED_RATES,
        payload: {
          currency,
          rates: result.rates,
        },
      },
    ];

    const store = mockStore({ exchange: { rates: [] } });

    return store.dispatch(actions.fetchRates(currency)).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
