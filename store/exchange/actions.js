import exchangeService from '../../services/exchangeService';
import * as types from './actionTypes';

export const fetchRates = currency => async (dispatch) => {
  try {
    dispatch({ type: types.FETCHING_RATES, payload: { currency } });
    const result = await exchangeService.getRates(currency);
    const { rates } = result;
    dispatch({ type: types.FETCHED_RATES, payload: { rates, currency } });
  } catch (error) {
    console.error(error);
    dispatch({ type: types.FETCHING_RATES_FAILED });
  }
};

export default fetchRates;
