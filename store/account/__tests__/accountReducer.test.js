import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { getWallets } from '../reducer';

const account = {
  wallets: [
    {
      title: 'EUR',
      value: 'EUR',
      symbol: '€',
      balance: 123.22,
    },
    {
      title: 'USD',
      value: 'USD',
      symbol: '$',
      balance: 0,
    },
    {
      title: 'GBP',
      value: 'GBP',
      symbol: '£',
      balance: 15,
    },
  ],
};

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions', () => {
  it('getWallets selector returns correct values', () => {
    const store = mockStore({ account });

    expect(getWallets(store.getState())).toEqual(account.wallets);
  });
});
