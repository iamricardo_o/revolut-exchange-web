import * as types from './actionTypes';

export const exchangeCurrencies = transaction => ({
  type: types.EXCHANGED_CURRENCIES,
  payload: { transaction },
});

export default exchangeCurrencies;
