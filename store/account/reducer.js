import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const roundNumber = num => Math.round(num * 100) / 100

const initialState = Immutable({
  wallets: [
    {
      title: 'EUR',
      value: 'EUR',
      symbol: '€',
      balance: 123.22,
    },
    {
      title: 'USD',
      value: 'USD',
      symbol: '$',
      balance: 0,
    },
    {
      title: 'GBP',
      value: 'GBP',
      symbol: '£',
      balance: 15,
    },
  ],
});

export default function reduce(state = initialState, action) {
  switch (action.type) {
    case types.EXCHANGED_CURRENCIES: {
      const { transaction } = action.payload;

      /* build new copy of wallet array with updated target wallet balance */
      const newWallets = state.wallets.map((wallet) => {
        if (wallet.value === transaction.target) {
          const newWallet = { ...wallet };

          newWallet.balance = roundNumber(wallet.balance + transaction.targetAmount);
          return newWallet;
        }

        if (wallet.value === transaction.source) {
          const newWallet = { ...wallet };

          newWallet.balance = roundNumber(wallet.balance - transaction.sourceAmount);
          return newWallet;
        }

        return wallet;
      });

      return Immutable.merge(state, {
        wallets: newWallets,
      });
    }

    default:
      break;
  }
  return state;
}

/* selectors */

export function getWallets(state) {
  return state.account.wallets;
}
