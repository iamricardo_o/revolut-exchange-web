## Install

Clone project and run

`yarn install`

## Running - Dev

To run project in dev environment

`yarn dev`

## Running - Prod

`yarn build` and then `yarn start`

You can find more details about running and deploying the project in the [Nextjs official repo]("https://github.com/zeit/next.js/").

On merge to master tests are run and if success app is deployed to https://stoic-shaw-84c8c2.netlify.com/
