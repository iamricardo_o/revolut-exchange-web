import { ButtonCircle } from 'rebass';
import styled from 'styled-components';

export const MiddleButton = styled(ButtonCircle)`
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  position: absolute;
  display: flex;
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.12);
  :active,
  :focus {
    outline: none;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }

  && .trend-icon {
    margin-right: 1em;
  }
`;

export default MiddleButton;
