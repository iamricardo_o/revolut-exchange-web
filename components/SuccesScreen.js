import PropTypes from 'prop-types';
import React from 'react';
import { ButtonCircle, Flex, Text } from 'rebass';
import styled from 'styled-components';
import Icon from './Icon';

const StyledIcon = styled(Icon)`
  color: ${props => props.theme.colors.blue};
`;

const Wrapper = styled.div`
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
`;

const SuccessScreen = ({ message, onDoneClick }) => (
  <Wrapper>
    <Flex justifyContent="center" align="center" mt={256} mb={3}>
      <StyledIcon name="far fa-check-circle" size={6} />
    </Flex>
    <Flex justifyContent="center" mb={6}>
      <Text>{message}</Text>
    </Flex>
    <Flex justifyContent="center">
      <ButtonCircle bg="pink" fontSize={2} onClick={onDoneClick}>
        Done
      </ButtonCircle>
    </Flex>
  </Wrapper>
);

SuccessScreen.propTypes = { message: PropTypes.string, onDoneClick: PropTypes.func };

SuccessScreen.defaultProps = {
  message: '',
  onDoneClick: null,
};

export default SuccessScreen;
