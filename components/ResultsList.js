import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: absolute;
  top: 66px;
  min-height: auto;
  max-height: 17em;
  overflow: auto;
  width: 100%;
  left: 0;
  padding: 0;
  background: #fff;
  z-index: 9999;
  box-shadow: 0 8px 10px 1px rgba(0, 0, 0, 0.14);
`;

const ResultWrapper = styled.div`
  cursor: pointer;
  &:hover {
    background-color: ${props => props.theme.colors.blue};
  }
`;

const Result = styled.span`
  padding: 1em;
  font-weight: bold;
  display: inline-block;
  width: 100%;
  &:hover {
    color: #fff;
  }
`;

const ResultsList = ({ results, onResultSelect, ...props }) => (
  <Wrapper {...props}>
    {results.map((result, i) => (
      <ResultWrapper key={`r-${result.value}`}>
        <Result onClick={() => onResultSelect(result)}>
          {result.title}
        </Result>
      </ResultWrapper>
    ))}
  </Wrapper>
);

ResultsList.propTypes = {
  results: PropTypes.arrayOf(PropTypes.shape({ title: PropTypes.string, value: PropTypes.any })),
  /** Function that is called when a result/option is clicked - the clicked result is passed as a param */
  onResultSelect: PropTypes.func.isRequired,
};

ResultsList.defaultProps = {
  results: [],
};

export default ResultsList;
