import { ButtonCircle } from 'rebass';
import styled from 'styled-components';

const ExchangeButton = styled(ButtonCircle)`
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);

  @media only screen and (min-width: 480px) {
    top: 75%;
    left: 50%;
    position: absolute;
    z-index: 20;
    transform: translate(-50%, -75%);
  }
`;

export default ExchangeButton;
