import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Text } from 'rebass';
import styled from 'styled-components';
import Icon from './Icon';
import OutsideAlerter from './OutsideAlerter';
import ResultsList from './ResultsList';

/* eslint-disable */
const ArrowIcon = styled(Icon)`
  position: absolute;
  z-index: 1;
  right: 8px;
  top: 50%;
  transform: translateY(-50%);
  color: ${props => props.theme.colors.black};
  cursor: pointer;
`;
/* eslint-enable */

const Wrapper = styled.div`
  position: relative;
  min-width: 66px;
  width: auto;
  padding: 9px 16px 9px 0;
  padding-right: 2em;
  cursor: pointer;
`;

class Dropdown extends Component {
  constructor(props) {
    super(props);
    const { options, defaultValue } = this.props;

    const defaultOption = options.filter(op => op.value === defaultValue);

    this.state = {
      isOpen: false,
      selectedOption: defaultOption[0] || null
    };
  }

  handleOptionChange = option => {
    const { onChange } = this.props;

    this.setState({ selectedOption: option }, onChange(option.value));
  };

  handleClick = () => {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  };

  handleClickOutsideDropdown = () => {
    this.setState({ isOpen: false });
  };

  render() {
    const { isOpen, selectedOption } = this.state;
    const { options, title, value } = this.props;
    return (
      <OutsideAlerter onClickOutside={this.handleClickOutsideDropdown}>
        <Wrapper onClick={this.handleClick} {...this.props}>
          {selectedOption &&
            !this.props.selectedOption && (
              <Text fontSize={4}>{selectedOption.title}</Text>
            )}
          {this.props.selectedOption && (
            <Text fontSize={4}>{this.props.selectedOption.title}</Text>
          )}

          {!selectedOption && <Text fontSize={4}>{title}</Text>}

          {!isOpen && (
            <ArrowIcon name="fas fa-chevron-down" onClick={this.handleClick} />
          )}
          {isOpen ? (
            <React.Fragment>
              <ResultsList
                onResultSelect={this.handleOptionChange}
                results={options}
              />
              <ArrowIcon name="keyboard_arrow_up" onClick={this.handleClick} />
            </React.Fragment>
          ) : null}
        </Wrapper>
      </OutsideAlerter>
    );
  }
}

Dropdown.propTypes = {
  /** Title of select box */
  title: PropTypes.string,
  /** Possible options and their values */
  options: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      value: PropTypes.any
    })
  ),
  /** Callback to retrieve value of option selected */
  onChange: PropTypes.func,
  defaultValue: PropTypes.string
};

Dropdown.defaultProps = {
  title: 'Select an option',
  options: [],
  onChange: value => console.log(value),
  defaultValue: '',
  /** passing this prop makes dropdown controlled */
  selectedOption: undefined
};

export default Dropdown;
