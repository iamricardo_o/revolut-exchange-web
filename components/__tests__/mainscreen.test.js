import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import { Provider as ThemeProvider } from 'rebass';
import theme from '../../config/theme';
import MainScreen from '../../pages/index';
import initStore from '../../store/index';

const store = initStore();

const ConnectedMainScreen = () => (
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <MainScreen />
    </Provider>
  </ThemeProvider>
);

describe('<MainScreen />', () => {
  const wrapper = mount(<ConnectedMainScreen />);

  it('renders <MainScreen /> ', () => {
    expect(wrapper.length).toBe(1);
  });

  it('renders <MainScreen /> with 2 Dropdown', () => {
    expect(wrapper.find('Dropdown').length).toBe(2);
  });
});
