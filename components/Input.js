import PropTypes from 'prop-types';
import React from 'react';
import { Input as Base } from 'rebass';
import styled, { css } from 'styled-components';

const StyledInput = styled(Base)`
  box-shadow: none;
  ${props => props.numbersOnly
    && css`
      text-align: right;
    `};
`;

const handleKeyPress = (e) => {
  const code = e.which ? e.which : e.keyCode;

  if (code != 46 && code > 31 && (code < 48 || code > 57)) {
    e.preventDefault();
  }
};

const Input = ({ numbersOnly, ...props }) => (
  <StyledInput
    fontSize={3}
    numbersOnly={numbersOnly}
    borderColor="transparent"
    {...props}
    onKeyPress={e => (numbersOnly ? handleKeyPress(e) : null)}
  />
);

Input.propTypes = {
  numbersOnly: PropTypes.bool,
};

Input.defaultProps = {
  numbersOnly: false,
};

export default Input;
