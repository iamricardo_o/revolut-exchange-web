import { Circle } from 'rebass';
import styled, { css } from 'styled-components';

export const transform = (props) => {
  if (props.rotate) {
    return css`
      transform: translateY(-50%) rotate(-90deg);
      @media only screen and (min-width: 480px) {
        transform: translate(-50%) rotate(-180deg);
      }
    `;
  }

  return css`
    transform: translateY(-50%) rotate(90deg);
    @media only screen and (min-width: 480px) {
      transform: translate(-50%) rotate(180deg);
    }
  `;
};

export const FlipButton = styled(Circle)`
  top: 50%;
  left: 2em;

  ${transform};
  position: absolute;
  z-index: 10;
  display: flex;
  justify-content: center;

  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.12);
  :active {
    outline: none;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }

  @media only screen and (min-width: 480px) {
    top: 25%;
    left: 50%;
  }
`;

export default FlipButton;
