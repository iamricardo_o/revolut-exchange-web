const theme = {
  fonts: {
    sans: '"Nunito Sans",  sans-serif',
  },
  fontSizes: [12, 16, 24, 36, 48, 72],
  colors: {
    blue: '#0075eb',
    pink: '#eb008d',
    darken: '#a0a6ad',
    gray: '#f3f4f5',
  },
};

export default theme;
