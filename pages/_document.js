import Document, { Head, Main, NextScript } from 'next/document';
import React from 'react';
import { injectGlobal, ServerStyleSheet } from 'styled-components';

injectGlobal`
html {
    min-height:100%;
    position:relative;
    max-height: 812px;
    position: relative;
    min-height: 100vh;
}

body, #__next, #__next > div {
  margin: 0;
  padding: 0;
  height:100%;
  position: relative;
  min-height: 100vh;
  }
`;

export default class MyDocument extends Document {
  static getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage(App => props => sheet.collectStyles(<App {...props} />));
    const styleTags = sheet.getStyleElement();
    return { ...page, styleTags };
  }

  render() {
    return (
      <html lang="en">
        <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

          <title>
Revolut - Exchange
          </title>
          {this.props.styleTags}
          <link
            href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,700"
            rel="stylesheet"
          />
          <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
            integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
            crossOrigin="anonymous"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
