import { debounce, find } from 'lodash';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Box, Flex, Text } from 'rebass';
import styled from 'styled-components';
import Dropdown from '../components/Dropdown';
import ExchangeButton from '../components/ExchangeButton';
import FlipButton from '../components/FlipButton';
import Icon from '../components/Icon';
import Input from '../components/Input';
import { MiddleButton } from '../components/MiddleButton';
import SuccessScreen from '../components/SuccesScreen';
import { exchangeCurrencies } from '../store/account/actions';
import { getWallets } from '../store/account/reducer';
import { fetchRates } from '../store/exchange/actions';
import { getRates } from '../store/exchange/reducer';

const Container = styled.div`
  height: 50%;
  width: 100%;
  padding: 4em 2em;
`;

const Top = styled(Container)``;

const Bottom = styled(Container)`
  background: ${props => props.theme.colors.gray};
`;

const Wrapper = styled.div`
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
`;

class Index extends PureComponent {
  constructor(props) {
    super(props);

    const { wallets } = this.props;
    this.state = {
      sourceCurrency: wallets[0],
      targetCurrency: wallets[2],
      source: '',
      target: '',
      rotate: false,
      isSuccess: false,
    };
  }

  componentDidMount() {
    this.InputRef.focus();

    this.ratesInterval = this.startTimer();
  }

  componentWillUnmount() {
    this.clearTimer();
  }

  startTimer = () => {
    this.doGetCurrencyRates();
    return setInterval(this.doGetCurrencyRates, 10000);
  };

  doGetCurrencyRates = () => {
    const { dispatch, wallets } = this.props;

    let len = wallets.length;
    while (len > 0) {
      len -= 1;
      const debouncedFetchRates = debounce(currency => dispatch(fetchRates(currency.value)), 500);
      debouncedFetchRates(wallets[len]);
    }
  };

  clearTimer = () => {
    clearInterval(this.ratesInterval);
  };

  setInputRef = (el) => {
    this.InputRef = el;
  };

  handleSourceCurrencyChange = (value) => {
    const { targetCurrency } = this.state;

    if (value === targetCurrency.value) {
      this.flipCurrencies();
    } else {
      const target = this.getCurrency(value);
      this.setState({ sourceCurrency: target });
    }
  };

  handleTargetCurrencyChange = (value) => {
    const { sourceCurrency } = this.state;

    if (value === sourceCurrency.value) {
      this.flipCurrencies();
    } else {
      const target = this.getCurrency(value);

      this.setState({ targetCurrency: target });
    }
  };

  flipCurrencies = () => {
    const { targetCurrency, sourceCurrency, rotate } = this.state;
    this.setState({
      sourceCurrency: targetCurrency,
      targetCurrency: sourceCurrency,
      rotate: !rotate,
    });
  };

  handleSourceValueChange = (inputValue) => {
    const { latestRates } = this.props;
    const { sourceCurrency, targetCurrency } = this.state;

    const rate = latestRates[`${sourceCurrency.value}`].rates[`${targetCurrency.value}`];

    const convertedAmount = (Number(inputValue) * rate).toFixed(2);
    this.setState({
      source: inputValue,
      target: convertedAmount,
    });
  };

  handleTargetValueChange = (inputValue) => {
    const { latestRates } = this.props;
    const { sourceCurrency, targetCurrency } = this.state;

    const rate = latestRates[`${targetCurrency.value}`].rates[`${sourceCurrency.value}`];

    const convertedAmount = (Number(inputValue) * rate).toFixed(2);
    this.setState({
      source: convertedAmount,
      target: inputValue,
    });
  };

  handleExchangeClick = () => {
    const { sourceCurrency, targetCurrency, target , source} = this.state;

    const { dispatch } = this.props;

    const transaction = {
      source: sourceCurrency.value,
      target: targetCurrency.value,
      sourceAmount: Number(source),
      targetAmount: Number(target),
    };

    dispatch(exchangeCurrencies(transaction));

    this.setState({ isSuccess: true });
  };

  resetState = () => {
    const { wallets } = this.props;

    const initialState = {
      sourceCurrency: wallets[0],
      targetCurrency: wallets[2],
      source: '',
      target: '',
      rotate: false,
      isSuccess: false,
    };

    this.setState(initialState);
  }

  getCurrency = (value) => {
    const { wallets } = this.props;
    return find(wallets, w => w.value === value);
  };

  render() {
    const {
      sourceCurrency, targetCurrency, source, target, rotate, isSuccess,
    } = this.state;
    const { latestRates, wallets } = this.props;

    if (isSuccess) {
      return (
        <SuccessScreen
          onDoneClick={this.resetState}
          message={`You exchanged ${sourceCurrency.value}${source} -> ${
            targetCurrency.value
          }${target}`}
        />
      );
    }

    return (
      <Wrapper>
        <Flex
          flexDirection={['column', 'row']}
          justifyContent="space-between"
          flex="1 1"
          style={{
            width: '100%',
            height: '100%',
            position: 'relative',
          }}
        >
          <Top>
            <Flex align="center" justify="space-between">
              <Box pr={3}>
                <Dropdown
                  options={wallets}
                  defaultValue={sourceCurrency.value}
                  onChange={this.handleSourceCurrencyChange}
                  selectedOption={sourceCurrency}
                />
                <Text color={sourceCurrency.balance < source ? 'pink' : 'darken'}>
                  Balance:
                  {' '}
                  {sourceCurrency.symbol}
                  {sourceCurrency.balance}
                </Text>
              </Box>
              <Box>
                <Input
                  numbersOnly
                  placeholder="0.00"
                  innerRef={this.setInputRef}
                  onChange={e => this.handleSourceValueChange(e.target.value)}
                  value={source}
                  pattern="^\d+(\.|\,)\d{2}$"
                />
              </Box>
            </Flex>
          </Top>
          <FlipButton onClick={this.flipCurrencies} rotate={rotate}>
            <Icon name="fas fa-exchange-alt" />
          </FlipButton>
          {latestRates
            && latestRates[`${sourceCurrency.value}`] && (
              <MiddleButton bg="white" color="blue">
                <Icon className="trend-icon" name="fas fa-chart-line" />
                <Text>
                  {sourceCurrency.symbol}
1 =
                  {targetCurrency.symbol}
                  {latestRates[`${sourceCurrency.value}`].rates[`${targetCurrency.value}`]}
                </Text>
              </MiddleButton>
          )}
          <Bottom>
            <Flex align="center" justify="space-between" mb={32}>
              <Box pr={3}>
                <Dropdown
                  options={wallets}
                  defaultValue={targetCurrency.value}
                  onChange={this.handleTargetCurrencyChange}
                  selectedOption={targetCurrency}
                />
                <Text color="darken">
                  Balance: {targetCurrency.symbol}
                  {targetCurrency.balance}
                </Text>
              </Box>
              <Box>
                <Input
                  numbersOnly
                  placeholder="0.00"
                  value={target}
                  onChange={e => this.handleTargetValueChange(e.target.value)}
                  pattern="^\d+(\.|\,)\d{2}$"
                />
              </Box>
            </Flex>
            <Flex justify="center">
              <ExchangeButton
                bg="pink"
                fontSize={2}
                disabled={
                  sourceCurrency.balance < source ||
                  source === '' ||
                  target === ''
                }
                onClick={this.handleExchangeClick}
              >
                Exchange
              </ExchangeButton>
            </Flex>
          </Bottom>
        </Flex>
      </Wrapper>
    );
  }
}

Index.propTypes = {
  latestRates: PropTypes.shape({}).isRequired,
  dispatch: PropTypes.func.isRequired,
  wallets: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

const mapStateToProps = state => ({
  latestRates: getRates(state),
  wallets: getWallets(state)
});

export default connect(mapStateToProps)(Index);
