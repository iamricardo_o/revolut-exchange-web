const BASE_URL = 'https://api.exchangeratesapi.io';

/**
 * Returns exchange rates for a given currency
 */
const getRates = async (currency) => {
  const url = `${BASE_URL}/latest?base=${currency}`;
  const response = await fetch(url);

  if (!response.ok) {
    console.log(response.Error);
  }

  const data = await response.json();

  return data;
};

export default {
  getRates,
};
